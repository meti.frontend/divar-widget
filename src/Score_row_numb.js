
import React from 'react';
import styled from 'styled-components'
import './App.css'
 


const Score_row_numb = (props) => {
    var b = 0 ;
    props.percentage_score >= 95 ? b = "20px" : b = "0";
    const Scorer = styled.div`
    width:${props.percentage_score}%;
    border-radius:20px ${b} ${b} 20px; 
    height:15px;
    `;
    return(  
        <div className="Score_row_numb">
            <span className="Score_row_line Score_row_line_N1 "></span>
            <span className="Score_row_line Score_row_line_N2"></span>
            <span className="Score_row_line Score_row_line_N3"></span>
            <span className="Score_row_line Score_row_line_N4"></span>
            <Scorer className={props.class}/>  
        </div>
    );
    
}
  

  export default Score_row_numb  ;