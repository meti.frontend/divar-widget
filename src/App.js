import './App.css';
import { Component } from 'react';
import Score_row_numb from './Score_row_numb';
import {FaAngleLeft} from 'react-icons/fa'

const array_widget_json = '[{"widget_type": "TITLE_ROW","data": {"text": "کیا اسپورتیج 2400cc","text_color": "TEXT_PRIMARY"}},{"widget_type": "UNEXPANDABLE_ROW","data": {"title": "تاریخ انجام کارشناسی","value": "۹۹/۰۵/۱۲"}},{"widget_type": "SECTION_DIVIDER_ROW"},{"widget_type": "SCORE_ROW","data": {"action": {"type": "SHOW_INSPECTION_ITEM","payload": {"slug": "YmFkYW5l"}},"title": "بدنه","percentage_score": 88,"score_color": "SUCCESS_PRIMARY","image_url": "https://s100.divarcdn.com/static/icons/4/ic_vehicles.png","image_color": "ICON_SECONDARY","has_divider": true,"icon": {"image_url_dark": "https://s100.divarcdn.com/static/imgs/widget-icons/dark/icon_secondary/car_body.png","image_url_light": "https://s100.divarcdn.com/static/imgs/widget-icons/light/icon_secondary/car_body.png","icon_name": "CAR_BODY","icon_color": "ICON_SECONDARY"}}},{"widget_type": "SCORE_ROW","data": {"action": {"type": "LOAD_PAGE","payload": {"slug": "Esdqwe232="}},"title": "لاستیک و رینگ‌ها","percentage_score": 60,"score_color": "WARNING_SECONDARY","image_url": "https://s100.divarcdn.com/static/icons/4/ic_car_tires.png","image_color": "ICON_SECONDARY","has_divider": true,"icon": {"image_url_dark": "https://s100.divarcdn.com/static/imgs/widget-icons/dark/icon_secondary/car_tires.png","image_url_light": "https://s100.divarcdn.com/static/imgs/widget-icons/light/icon_secondary/car_tires.png","icon_name": "CAR_TIRES","icon_color": "ICON_SECONDARY"}}}]'
let array_widget = JSON.parse(array_widget_json)






// // //  const widget_json = '{ "widget_type": "FEATURE_ROW", "data": { "title": "بررسی فنی بیش از ۴۰۰ نقطه‌ خودرو", "image_url": "https://s100.divarcdn.com/static/icons/4/ic_checked.png", "image_color": "SUCCESS_PRIMARY", "has_divider": true, "icon": { "image_url_dark": "https://s100.divarcdn.com/static/imgs/widget-icons/dark/success_primary/checked.png", "image_url_light": "https://s100.divarcdn.com/static/imgs/widget-icons/light/success_primary/checked.png", "icon_name": "CHECKED", "icon_color": "SUCCESS_PRIMARY" } } }'
// // //  let widget = JSON.parse(widget_json);

// // // const widget_json2 = '{ "widget_type": "SCORE_ROW", "data": { "title": "بدنه", "percentage_score": 88, "score_color": "SUCCESS_PRIMARY", "image_url": "https://s100.divarcdn.com/static/icons/4/ic_vehicles.png", "image_color": "ICON_SECONDARY", "action": { "type": "SHOW_INSPECTION_ITEM", "payload": { "type": "type1", "slug": "YmFkYW5l" } }, "has_divider": true, "icon": { "image_url_dark": "https://s100.divarcdn.com/static/imgs/widget-icons/dark/icon_secondary/car_body.png", "image_url_light": "https://s100.divarcdn.com/static/imgs/widget-icons/light/icon_secondary/car_body.png", "icon_name": "CAR_BODY", "icon_color": "ICON_SECONDARY" } } }'
// // // let widget2 = JSON.parse(widget_json2);

// // const SHOW_INSPECTION_ITEM = '{"widget_type": "DESCRIPTION_ROW","data": {"title": "بدنه","text": "کاپوت سنگ خوردگی دارد. به دلیل وجود فلاپ امکان بررسی رکاب ها وجود ندارد.","has_divider": true}}'
// // let SHOW_INSPECTION_ITEM1 = JSON.parse(SHOW_INSPECTION_ITEM);



class App extends Component{



  render() {
    return(
      <div>
        <p className={`App-intro ${array_widget[0].widget_type}`}>
          <h3>{array_widget[0].data.text}</h3>
          <section className={array_widget[1].widget_type}>
            <h3 className="UNEXPANDABLE_ROW_title">{array_widget[1].data.title}</h3>
            <h3 className="UNEXPANDABLE_ROW_value">{array_widget[1].data.value}</h3>
          </section>
        </p>
        <p className='App-intro' >
        <section className="Title_SCORE_ROW">
            <img className="ICON_SCORE_ROW" src={array_widget[3].data.icon.image_url_light}/>
            <h4 className="TEXT_SCORE_ROW" >{array_widget[3].data.title}</h4>
          </section>
          <section className="percentage_score">
            <Score_row_numb class={array_widget[3 ].data.score_color} percentage_score={array_widget[3].data.percentage_score}/>
            <FaAngleLeft className="FaAngleLeft"/>
          </section>
        </p>
        <p className='App-intro'>
          <section className="Title_SCORE_ROW">
            <img className="ICON_SCORE_ROW" src={array_widget[4].data.icon.image_url_light}/>
            <h4 className="TEXT_SCORE_ROW" >{array_widget[4].data.title}</h4>
          </section>
          <section className="percentage_score">
            <Score_row_numb class={array_widget[4].data.score_color} percentage_score={array_widget[4].data.percentage_score}/>
            <FaAngleLeft className="FaAngleLeft"/>
          </section>
        </p>
      </div>
);
}
}

export default App;
